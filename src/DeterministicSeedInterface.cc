#include "Base/Modules/interface/DeterministicSeedInterface.h"

#include <openssl/sha.h>

DeterministicSeedInterface::DeterministicSeedInterface() {}

uint64_t DeterministicSeedInterface::create_seed(uint64_t val, int n_hex = 16)
{
    // Convert integer to string
    std::string val_str = std::to_string(val);

    // Compute SHA-256 hash
    unsigned char hash[SHA256_DIGEST_LENGTH];
    SHA256(reinterpret_cast<const unsigned char*>(val_str.c_str()), val_str.size(), hash);

    // Convert hash to hexadecimal string
    std::ostringstream hex_stream;
    for (int i = 0; i < SHA256_DIGEST_LENGTH; ++i) {
        hex_stream << std::hex << std::setw(2) << std::setfill('0') << static_cast<int>(hash[i]);
    }
    std::string hex_str = hex_stream.str();

    // Extract the last n_hex characters
    std::string extracted_hex = hex_str.substr(hex_str.size() - n_hex);

    // Reverse the extracted_hex to preserve c++ <--> python compatiblity
    std::reverse(extracted_hex.begin(), extracted_hex.end());

    // Convert the extracted hex string to uint64_t
    uint64_t result = std::stoull(extracted_hex, nullptr, 16);

    return result;
}


uint64_t DeterministicSeedInterface::compute_seed(
    long int event, int run, int luminosityBlock,
    int Pileup_nPU, int nJet, int nFatJet, int nSubJet,
    int nMuon, int nElectron, int nTau, int nSV, int nGenJet,
    iRVec Electron_jetIdx, iRVec Electron_seediPhiOriY, iRVec Muon_jetIdx,
    iRVec Muon_nStations, iRVec Tau_jetIdx, iRVec Tau_decayMode,
    iRVec Jet_nConstituents, iRVec Jet_nElectrons, iRVec Jet_nMuons)
{
    std::vector<int> index_input = {(int)event, run, luminosityBlock};
    
    std::vector<int> event_inputs = {Pileup_nPU, nJet, nFatJet, nSubJet,
                                     nMuon, nElectron, nTau, nSV,
                                     nGenJet};

    std::vector<iRVec> object_inputs = {Electron_jetIdx, Electron_seediPhiOriY,
                                        Muon_jetIdx, Muon_nStations, Tau_jetIdx,
                                        Tau_decayMode, Jet_nConstituents,
                                        Jet_nElectrons, Jet_nMuons};

    // Start by creating a short seed from index inputs
    uint64_t event_seed = create_seed(
                                index_input[0] * primes[7] + 
                                index_input[1] * primes[5] + 
                                index_input[2] * primes[3],
                                14
                            );

    // Fold with event-level info
    int value_offset = 3;
    int prime_offset = 15;
    for (size_t i = 0; i < event_inputs.size(); ++i) {
        int inp = event_inputs[i] + i + value_offset;
        event_seed += primes[(inp + prime_offset) % primes.size()] * inp;
    }

    // Fold with object-level info
    for (size_t i = 0; i < object_inputs.size(); ++i) {
        int inp_sum = object_inputs[i].size();
        for (size_t j = 0; j < object_inputs[i].size(); ++j) {
            int inp = object_inputs[i][j] + i + value_offset;
            inp_sum += inp * (j + 1) + std::pow(inp, 2) * (j + 1);
        }
        event_seed += primes[(inp_sum + prime_offset) % primes.size()] * inp_sum;
    }

    // Final seed
    return create_seed(event_seed);
}

uint64_t DeterministicSeedInterface::object_seed(uint64_t event_seed, int obj_idx, int offset) {
    uint64_t seed = event_seed + primes[event_seed % primes.size()] * (obj_idx + primes[offset]);
    return create_seed(seed);
}
